function fetchCocktails(type) {
    const apiUrl = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${type}`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            const cocktailContainer = document.getElementById('cocktailContainer');
            cocktailContainer.innerHTML = '';
            totalCocktails = data.drinks.length;

            data.drinks.forEach(cocktail => {
                const cocktailDiv = document.createElement('div');
                cocktailDiv.classList.add('cocktail');
                const cocktailImage = document.createElement('img');
                cocktailImage.src = cocktail.strDrinkThumb;
                cocktailImage.alt = cocktail.strDrink;

                const cocktailName = document.createElement('p');
                cocktailName.classList.add('drinkName');
                cocktailName.textContent = cocktail.strDrink;

                cocktailDiv.appendChild(cocktailImage);
                cocktailDiv.appendChild(cocktailName);
                cocktailContainer.appendChild(cocktailDiv);
            });

            document.getElementById('totalCocktails').textContent = `Total de Cocteles: ${totalCocktails}`;
            document.getElementById('cocktails').style.display = 'block';
            document.getElementById('clearBtn').style.display = 'block';
        })
        .catch(error => {
            console.error('Error fetching cocktails:', error);
            alert('An error occurred while fetching the cocktails. Please try again later.');
        });
}

function clearScreen() {
    const cocktailContainer = document.getElementById('cocktailContainer');
    cocktailContainer.innerHTML = '';
    document.getElementById('cocktails').style.display = 'none';
    document.getElementById('totalCocktails').textContent = 'Total de Cocteles: 0';
    document.getElementById('clearBtn').style.display = 'none';
}